# image2oscilloscope

This is a collections of Interactive Python Notebooks that I use to generate signals to recreate images in my analog oscilloscope.

## Requirements

You will need:

* Python 3.9.7 or similar
* opencv
* opencv-contrib-python
* the pip packages in the `requirements.txt` file
* JupyterLab, alternatively you can use Google Colab
* Arduino IDE and an Arduino DUE (you can use any microcontroller but you will need to make changes in the code)
* BNC cables and some jumpers/wires
* Analog oscilloscope

### Installing programs

Install [Python 3](https://www.python.org/downloads/) on your OS, check the version with: `python3 -V`

Install [JupyterLab](https://github.com/jupyterlab/jupyterlab-desktop) in your OS, there is also a JupyterLab version that runs in your browser.

Install OpenCV in your OS (check the proper instructions),  in the case of `Ubuntu` just run:
```
sudo apt install libopencv-dev python3-opencv
```

### Creating a virtual environment

I recommend to use a `virtual environment`. Installing the packages in the main Python environment can cause conflicts with other packages.

#### asciinema

Check this terminal session using ArcoLinux to help you in the process.

[![asciicast](https://asciinema.org/a/473991.svg)](https://asciinema.org/a/473991)

The recommended way to create a virtual environment is to use the `venv` module.

The `python3-venv` package provides the `venv` module.

```
sudo apt install python3-venv
```

Switch to the directory where you would like to store your Python 3 virtual environments (maybe `$HOME/venv` o `$HOME/.venvs`). Within the directory run the following command to create your new virtual environment:

```
python3 -m venv image2oscilloscope
```

The command above creates a directory called `image2oscilloscope`, which contains a copy of the Python binary, the Pip package manager, the standard Python library and other supporting files.

To start using this virtual environment, you need to activate it by running the activate script:

```
source image2oscilloscope/bin/activate
```

Once activated, the virtual environment’s bin directory will be added at the beginning of the `$PATH` variable. Also your shell’s prompt will change and it will show the name of the virtual environment you’re currently using. If you use `bash` it looks like:

```
$ source image2oscilloscope/bin/activate
(image2oscilloscope) $
```
Run `which python` and you will see something like: `$HOME/.venvs/image2oscilloscope/bin/python`

Move to folder in which you want to clone this repo (is not the same as `$HOME/.venvs/image2oscilloscope`). Example: `cd ~/Documents/Python`

```
(image2oscilloscope) $ git clone https://gitlab.com/hasecilu/image2oscilloscope.git
```

Now, with the virtual environment activated, we can start install the needed packages, copy the `requeriments.txt`

```
(image2oscilloscope) $ pip install -r requirements.txt
```

#### Creating the kernel

In order to be able to use this environment with JupyterLab we need to run the command below, (the necessary packages were already installed wit the requirements file)

```
(image2oscilloscope) $ ipython kernel install --user --name=image2oscilloscopeKernel
```

Once you are done with your work to deactivate the environment, simply type deactivate and you will return to your normal shell.

```
(image2oscilloscope) $ deactivate
```

## Usage

Open JupyterLab and then open the 3 notebooks. In the up-right corner clic in the `Switch kernel` button and choose the `image2oscilloscopeKernel` kernel.

The file `1_bin_edge.ipynb` transform a image (ideally a drawing) into a binary image and a then passes the image through an edge detection.

The file `2_skeleton.ipynb` creates a skeleton (1 pixel-width edges) of a bin image (ideally thick strokes).

The file `3_points.ipynb` creates the data points from a 1 pixel-width image strokes.

Maybe you will need to edit the resultant images with an image editor like [GIMP](https://www.gimp.org/) from 1 to 2 and from 2 to 3.

In the codes there are different algorithms that requires some parameters, if the image that you are trying to process don't result as expected try changing those values.

Once you generated your header file just compile the Arduino code and upload it to your Arduino Due.

### Tips

* It is totally recommended to use an CRT screen oscilloscope, it could be an analog oscilloscope or and DSO oscilloscope one. Check this [video](https://www.youtube.com/watch?v=mG3enGpGyNA) from  Jacob Dykstra to understand the differences.
* Use small images, depending on the specs of your oscilloscope you can be limited in the number of samples you can show in the screen of your oscilloscope.
* Header files with 5000 or less points are recommended for analog oscilloscopes, with DSO ones you can use more points.

## Images

You can generate data for:

* static images
* animations

NAVI, play track 44!

![CYBERIA](images/CYBERIA.jpg)

![lain_dancing](images/lain_dancing.gif)

