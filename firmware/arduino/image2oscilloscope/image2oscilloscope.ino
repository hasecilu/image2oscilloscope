/* *************************************************************************************************
 *  Sketch: image2oscilloscope
 *  Author: @hasecilu
 *  Date: 2021-12-25
 *  Description: This sketch produces a X & Y output to DAC0  and DAC1 of the arduino DUE board. 
 *  The data points are in the data.h header file and contains the (x, y) coordinates to reproduce 
 *  a drawing in the oscilloscope (preferably an analog one).
 *
 * *************************************************************************************************/

#include "data.h"         // Header file with data points

#define DAC_RESOLUTION 10 //0-1023
#define K 1               // The writed data is cut k times, k=2 -> half the data

int sample = 0;
#ifdef FRAMES
  int frame = 0;
  int repet = 0;
  #define REP 6           // Number of times every simgle frame wil be printed
#endif


void setup() {
  analogWriteResolution(DAC_RESOLUTION);
  analogReadResolution(DAC_RESOLUTION);
}

void loop() {
  #ifndef FRAMES
  
    analogWrite(DAC0, pointData[sample][X]);
    analogWrite(DAC1, pointData[sample][Y]);
    //delayMicroseconds(5);

    if (sample > DATA_LENGTH)
      sample = 0;
    else
      sample += K;

  #else
  
    analogWrite(DAC0, pointData[frame][sample][X]);
    analogWrite(DAC1, pointData[frame][sample][Y]);

    if (sample > frameSize[frame] - 1){
      sample = 0;
      if (repet == REP){
        repet = 0;
        frame++;
      }
      if (frame == FRAMES)
        frame = 0;
      else
        repet++;
    }
    else
      sample += K;
  
  #endif
}

