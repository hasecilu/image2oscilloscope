# firmware

The code is the same but I prefer to use PlatformIO rather than the Arduino IDE.

## Requirements

You will need:

* Arduino IDE or VS code (VS codium) + PlatformIO extension
* Arduino DUE (you can use any microcontroller but you will need to make changes in the code)
* BNC cables and some jumpers/wires
* Analog oscilloscope

## Usage

* Generate the data points with the `3_points.ipynb` file
* Compile the code using your favorite IDE
* Upload the hex file to the Arduino DUE
* Connect to the oscilloscope
